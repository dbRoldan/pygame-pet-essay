# -*- coding: utf-8 -*-
import sys, pygame, os
from pygame.sprite import Sprite
from pygame.locals import *

class Character(Sprite):
    def __init__(self, name, images, pos, vel, stepsTaken, maxSize):
        Sprite.__init__(self)
        self.images = images
        self.pos = pos
        self.name = name
        self.vel = vel
        self.stepsTaken = stepsTaken
        self.currentStep = ''
        self.initImage = self.loadImage(self.images['init'])
        self.rect = self.initImage.get_rect()
        self.rect.move_ip(self.pos)
        self.moveUp = self.imageSpriteMove(self.images['back'])
        self.moveDown = self.imageSpriteMove(self.images['front'])
        self.moveRight = self.imageSpriteMove(self.images['right'])
        self.moveLeft = self.imageSpriteMove(self.images['left'])
        self.moveTouch = self.imageSpriteMove(self.images['touch'])
        self.changeMessage = False
        self.maxX, self.maxY  = maxSize

    def getInitSurface(self):
        return self.initImage

    def getChangeMessage(self):
        return self.changeMessage

    def setChangeMessage(self, change):
        self.changeMessage = change

    def move(self, event):
        movementsMade = []
        if event.type == pygame.KEYDOWN:
            x, y = self.pos
            if event.key == pygame.K_LEFT:
                movementsMade = 'LEFT'
                #print('LEFT pressed')
                x -= self.vel
                if x < 0:
                    x = self.maxX
            elif event.key == pygame.K_RIGHT:
                movementsMade = 'RIGHT'
                #print('Right pressed')
                x += self.vel
                if x > self.maxX:
                    x = 0
            elif event.key == pygame.K_UP:
                movementsMade = 'UP'
                #print('Up pressed')
                y -= self.vel
                if y < 0:
                    y = self.maxY
            elif event.key == pygame.K_DOWN:
                movementsMade = 'DOWN'
                #print('Down Pressed')
                y += self.vel
                if y > self.maxY:
                    y = 0
            elif event.key == pygame.K_m:
                movementsMade = 'TOUCH'
                self.changeMessage = True if self.changeMessage == False else False
                print('Message!')
            elif event.key == pygame.K_ESCAPE:
                sys.exit()
            self.pos = x, y
        self.stepsTaken+= 1
        return movementsMade

    def setWindowPosition(self, movementMade):
        moveArray = self.moveRight
        if movementMade:
            self.currentStep = movementMade

        if self.currentStep  == 'LEFT':
            moveArray = self.moveLeft
        elif self.currentStep == 'RIGHT':
            moveArray = self.moveRight
        elif self.currentStep == 'UP':
            moveArray = self.moveUp
        elif self.currentStep == 'DOWN':
            moveArray = self.moveDown
        elif self.currentStep == 'TOUCH':
            moveArray = self.moveTouch
        surface = self.verificationPosition(moveArray)
        return surface, self.pos

    def verificationPosition(self, moveArray):
        if (self.stepsTaken) >= len(moveArray):
            self.stepsTaken = 0
        return moveArray[self.stepsTaken]

    def loadImage(self, filename):
        if os.path.isfile(filename):
            image = pygame.image.load(filename)
            return image
        else:
            raise Exception ("Sprite Error: Image in " + filename + " not found!")

    def imageSpriteMove(self, images):
        move = []
        for image in images:
            move.append(self.loadImage(image))
        return move
