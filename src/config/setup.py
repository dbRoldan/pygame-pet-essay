#!/usr/bin/python
# -*- coding: utf-8 -*-
 
from cx_Freeze import setup, Executable
 
setup(
 name="Pet Soft",
 version="0.1",
 description="Delivery messages software",
 executables = [Executable("app.py")],
 )
