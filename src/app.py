# -*- coding: utf-8 -*-
import  pygame

import json
from models.CharacterModel import Character


# Configuration Properties
configRoute = 'config/config.json'
with open(configRoute) as json_file:
    config = json.load(json_file)

# Behaviour Properties
vel = 15
stepsTaken = 0

# Window Properties
configWin = config['sizeWindow']
size = configWin['width'], configWin['height']
window = pygame.display.set_mode(size, pygame.NOFRAME) #0, 32)
pygame.display.set_caption(config['captionWindow'])
pygame.event.set_blocked(pygame.MOUSEMOTION)
pygame.event.set_blocked(pygame.MOUSEBUTTONDOWN)
pygame.event.set_blocked(pygame.MOUSEBUTTONUP)

# Character Properties
configChar = config['character']
name =  configChar['name']
images = configChar['images']
charSize = configChar['size']['width'], configChar['size']['height']
initPos = (configChar['initPos']['x'], configChar['initPos']['y'])
character = Character(name, images, initPos, vel, stepsTaken, (size) )

# Poster Properties
configMsg = config['poster']
wMsg, hMsg = configMsg['size']['width'], configMsg['size']['height']


#clock = pygame.time.Clock()
bgd = pygame.image.load('assets/bg.jpg')
background = pygame.transform.scale(bgd, (size))

def main():
    print('Pet window started!, Hi, i\'m ' + config['character']['name'])
    pygame.init()
    #clock.tick(1000)
    fontMsg = pygame.font.SysFont("Arial", 16, True, False)
    pygame.time.delay(1000)

    # Behaviour objects
    pause = False
    charSurface, charPos = character.getInitSurface(),  initPos
    message = 'Hola!,'
    color = (255, 255, 255)
    auxMsg = fontMsg.render('ESC, flechas o \'m\'', 1, color)
    allMsg = configMsg['messages']
    iMsg = 0

    while not pause:
        window.blit(background, (0, 0))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            else:
                movementMade = character.move(event)
                charSurface, charPos = character.setWindowPosition(movementMade)
        window.blit(charSurface, charPos)

        #Message
        xMsg, yMsg = charPos
        xMsg = xMsg - wMsg + 5
        pygame.draw.rect(window, color, (xMsg, yMsg, wMsg, hMsg), 3)
        if character.getChangeMessage():
            message = allMsg[iMsg]
            if iMsg+1 == len(allMsg):
                iMsg = 0
            else:
                iMsg += 1
            character.setChangeMessage(False)
        finalMsg = fontMsg.render(message, 1, color)
        window.blit(finalMsg, (xMsg + 8, yMsg + 8))
        window.blit(auxMsg, (xMsg + 10, yMsg + 25))
        pygame.display.update()
    pygame.quit()
if __name__ == "__main__":
    main()
